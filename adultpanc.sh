#!/bin/bash -i
#SBATCH --job-name=adultpanc
#SBATCH --time=14-00:00:00
#SBATCH --mail-user=Mohamed.soudy@uni.lu
#SBATCH --mail-type=ALL 
#SBATCH --output=adultpanc.out	
#SBATCH --error=adultpanc.%J.stdout
#SBATCH --output=adultpanc.%J.stderr
#SBATCH -p bigmem
#SBATCH -c 15 
cd /work/projects/cell_type_annotation/
conda activate rstudio-env
Rscript --no-save --no-restore --vanilla benchmarking_script.R
