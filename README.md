# sccca: Single-Cell Correlation Based Cell Type Annotation

[![CRAN RStudio mirror downloads](https://cranlogs.r-pkg.org/badges/grand-total/sccca?color=blue)](https://CRAN.R-project.org/package=sccca) 
[![CRAN RStudio mirror downloads](https://cranlogs.r-pkg.org/badges/sccca)](https://CRAN.R-project.org/package=sccca) 
[![](https://www.r-pkg.org/badges/version/sccca?color=green)](https://CRAN.R-project.org/package=sccca) 

# Installation

- Install from CRAN 
```R
install.packages("sccca")
```
- Install from GitLab
```R
library(devtools)
install_gitlab("mohamed.soudy/sccca")
```

# Replication 

- RUN the `adultpanc.R` to get the adult pancreatic data 
- Pass the output Seurat object of the `adultpanc.R` script to the `benchmarking_script.R`

# Usage

**Example**

**Parameters**

- sobj: Seurat object.

- assay: assay to be used default is set to RNA.

- cluster: colname in the mata.data that have the cell cluster numbers.

- marker: cell markers database path.

- tissue: specified tissue from which the data comes.

- tt: tissue type whether 'a' for all types 'n' for normal tissues only or "c" for cancer tissues.

- cond: colname in the meta.data that have the condition names.

- m_t: overlap threshold between cell markers and expression matrix.

- c_t: correlation threshold between genes.

- test: statistical test that check if overlap is significant could be "p" for phyper or "f" for fisher.

- org: organism to be used that can be 'h' for human, 'm' for mouse, and 'a' for all markers.


```R
library(sccca)
library(SeuratObject)
library(SeuratData)
library(Seurat)
# Adult Pancreatic data by Enge, Martin, et al.
adultpanc <- LoadSeuratRds("use-case/adult-pancreas/Normalized_PCA.Rds")
cluster<- adultpanc@meta.data$cluster
adultpanc@meta.data$cluster <- adultpanc@meta.data$PC12K7

sobj_markers <- sccca(adultpanc, assay = "RNA", cluster = 'cluster',
                      cond = NULL, marker = "UMD/www/Unified_markers_db.csv",tt = "n",
                      tissue = "Pancreas", m_t = 0.8, c_t = 0.8,
                      test = "p", org = "h")

#Get the Seurat object                      
sobj <- sobj_markers[[1]]
#Prepare for visualization
cell_type <- factor(sobj@meta.data$cell_type)
names(cell_type) <- sobj@meta.data$cell_id
sobj@active.ident <- cell_type
sobj <- RunUMAP(sobj, dims = 1:12)
DimPlot(sobj, reduction = "umap")

# PBMC3K data by Zheng, Grace XY, et al
pbmc <- LoadData('pbmc3k')
# normalize data
pbmc[["percent.mt"]] <- PercentageFeatureSet(pbmc, pattern = "^MT-")
# pbmc <- subset(pbmc, subset = nFeature_RNA > 200 & nFeature_RNA < 2500 & percent.mt < 5) # make some filtering based on QC metrics visualizations, see Seurat tutorial: https://satijalab.org/seurat/articles/pbmc3k_tutorial.html
pbmc <- NormalizeData(pbmc, normalization.method = "LogNormalize", scale.factor = 10000)
pbmc <- FindVariableFeatures(pbmc, selection.method = "vst", nfeatures = 2000)

# scale and run PCA
pbmc <- ScaleData(pbmc, features = rownames(pbmc))
pbmc <- RunPCA(pbmc, features = VariableFeatures(object = pbmc))

# Check number of PC components (we selected 10 PCs for downstream analysis, based on Elbow plot)
ElbowPlot(pbmc)

# cluster and visualize
pbmc <- FindNeighbors(pbmc, dims = 1:10)
pbmc <- FindClusters(pbmc, resolution = 0.8)
pbmc <- RunUMAP(pbmc, dims = 1:10)
DimPlot(pbmc, reduction = "umap")
#Sctype approach
pbmc_rep <- sctype(sobj = pbmc, assay = "RNA", tissue = "Immune system", tt = "n", clus = "seurat_clusters", org = "a", scaled = T, database = "sctype")
DimPlot(pbmc_rep, reduction = "umap", group.by = 'cell_type')  
```
